package com.junit.assign.test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.junit.assign.Book;

@RunWith(Parameterized.class)
public class BookTest {

	private float price;
	private float discount;
	private float finalPrice;
	Book book=new Book();
	public BookTest(float price,float discount,float finalPrice)
	{
		this.price=price;
		this.discount=discount;
		this.finalPrice=finalPrice;
	}
	 @SuppressWarnings("rawtypes")
	@Parameters
	   public static Collection primeNumbers() {
	      return Arrays.asList(new Object[][] {
	         { new Integer(100),new Integer(10),new Integer(81) },
	         { new Integer(1000), new Integer(10),new Integer(810) },
	        
	      });
	   }
	 @Test
	 public void bookTest()
	 {
		 assertEquals(finalPrice, 
			      book.doubleDiscount(price,discount),0.1F);
	 }
}
